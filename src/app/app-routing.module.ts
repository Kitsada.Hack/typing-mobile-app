import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'pre-home',
    loadChildren: () => import('./modules/pre-home/pre-home.module').then(m => m.PreHomeModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'pre-home/first',
    pathMatch: 'full'
  },
  {
    path: 'hardware',
    loadChildren: () => import('./modules/hardware-session/hardware-session.module').then(m => m.HardwareSessionModule)
  },
  {
    path: 'software',
    loadChildren: () => import('./modules/software-session/software-session.module').then(m => m.SoftwareSessionModule)
  },
  {
    path: 'data',
    loadChildren: () => import('./modules/data-session/data-session.module').then(m => m.DataSessionModule)
  },
  {
    path: 'people',
    loadChildren: () => import('./modules/people-session/people-session.module').then(m => m.PeopleSessionModule)
  },
  {
    path: 'test',
    loadChildren: () => import('./modules/test-session/test-session.module').then(m => m.TestSessionModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
