import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestSessionMainComponent } from './components/test-session-main/test-session-main.component';

const routes: Routes = [
  {
    path: "",
    component: TestSessionMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestSessionRoutingModule { }
