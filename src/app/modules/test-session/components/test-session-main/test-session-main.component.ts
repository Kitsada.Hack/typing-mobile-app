import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-test-session-main',
  templateUrl: './test-session-main.component.html',
  styleUrls: ['./test-session-main.component.scss'],
})
export class TestSessionMainComponent implements OnInit {

  exercise: any;
  exerciseList: any = [];
  radioSelected: any;
  disableRadio: Boolean = false;
  totalPoint: any = 0;
  currentSection: any = 0;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.exercise = null;
    this.exerciseList = [];
    this.totalPoint = 0;
    this.disableRadio = false;
    this.radioSelected = null;
    this.currentSection = 0;
    this.initialData();
  }

  initialData() {
    this.httpClient
      .get("assets/test.json")
      .subscribe(data => {
        this.exerciseList = data;
        this.exercise = data[0];
        this.currentSection = 0;
    })
  }

  onItemChange(choice: Number) {
    // console.log(choice);
  }

  checkAnswer() {
    if (this.radioSelected >= 0) {
      this.disableRadio = true;
      if (this.exercise.answer === this.radioSelected) {
        this.exercise.choice[this.radioSelected].color = "green";
        this.totalPoint = this.totalPoint + 1;
      } else {
        this.exercise.choice[this.exercise.answer].color = "green";
        this.exercise.choice[this.radioSelected].color = "red";
      }
    }
  }

  nextSection() {
    if (this.disableRadio) {
      this.reInitialCondition();
      this.currentSection = this.currentSection + 1;
      this.exercise = this.exerciseList[this.currentSection];
    }
  }

  reInitialCondition() {
    this.exercise = null;
    this.disableRadio = false;
    this.radioSelected = null;
  }

}
