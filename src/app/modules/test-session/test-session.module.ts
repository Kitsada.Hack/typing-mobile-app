import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestSessionRoutingModule } from './test-session-routing.module';
import { TestSessionMainComponent } from './components/test-session-main/test-session-main.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TestSessionMainComponent
  ],
  imports: [
    CommonModule,
    TestSessionRoutingModule,
    HttpClientModule,
    FormsModule
  ]
})
export class TestSessionModule { }
