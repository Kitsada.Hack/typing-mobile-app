import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hardware-session-main',
  templateUrl: './hardware-session-main.component.html',
  styleUrls: ['./hardware-session-main.component.scss'],
})
export class HardwareSessionMainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {}

  onSelectSession(path: any) {
    this.router.navigate([`/${path}`]);
  }
}
