import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HardwareSessionRoutingModule } from './hardware-session-routing.module';
import { HardwareSessionMainComponent } from './components/hardware-session-main/hardware-session-main.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
    HardwareSessionMainComponent
  ],
  imports: [
    FormsModule,
    IonicModule,
    CommonModule,
    HardwareSessionRoutingModule
  ]
})
export class HardwareSessionModule { }
