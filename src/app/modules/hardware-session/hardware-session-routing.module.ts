import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HardwareSessionMainComponent } from './components/hardware-session-main/hardware-session-main.component';

const routes: Routes = [
  {
    path: "",
    component: HardwareSessionMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HardwareSessionRoutingModule { }
