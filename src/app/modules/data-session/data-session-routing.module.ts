import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataSessionMainComponent } from './components/data-session-main/data-session-main.component';

const routes: Routes = [
  {
    path: "",
    component: DataSessionMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataSessionRoutingModule { }
