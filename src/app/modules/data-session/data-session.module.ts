import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataSessionRoutingModule } from './data-session-routing.module';
import { DataSessionMainComponent } from './components/data-session-main/data-session-main.component';


@NgModule({
  declarations: [
    DataSessionMainComponent
  ],
  imports: [
    CommonModule,
    DataSessionRoutingModule
  ]
})
export class DataSessionModule { }
