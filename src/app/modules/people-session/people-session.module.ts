import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeopleSessionRoutingModule } from './people-session-routing.module';
import { PeopleSessionMainComponent } from './components/people-session-main/people-session-main.component';


@NgModule({
  declarations: [
    PeopleSessionMainComponent
  ],
  imports: [
    CommonModule,
    PeopleSessionRoutingModule
  ]
})
export class PeopleSessionModule { }
