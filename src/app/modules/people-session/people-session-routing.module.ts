import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeopleSessionMainComponent } from './components/people-session-main/people-session-main.component';

const routes: Routes = [
  {
    path: "",
    component: PeopleSessionMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleSessionRoutingModule { }
