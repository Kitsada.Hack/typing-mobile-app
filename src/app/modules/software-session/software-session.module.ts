import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SoftwareSessionRoutingModule } from './software-session-routing.module';
import { SoftwareSessionMainComponent } from './components/software-session-main/software-session-main.component';


@NgModule({
  declarations: [
    SoftwareSessionMainComponent
  ],
  imports: [
    CommonModule,
    SoftwareSessionRoutingModule
  ]
})
export class SoftwareSessionModule { }
