import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-software-session-main',
  templateUrl: './software-session-main.component.html',
  styleUrls: ['./software-session-main.component.scss'],
})
export class SoftwareSessionMainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {}

  onSelectSession(path: any) {
    this.router.navigate([`/${path}`]);
  }
}
