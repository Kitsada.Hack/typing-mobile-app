import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SoftwareSessionMainComponent } from './components/software-session-main/software-session-main.component';

const routes: Routes = [
  {
    path: "",
    component: SoftwareSessionMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoftwareSessionRoutingModule { }
