import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreHomeRoutingModule } from './pre-home-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PreHomeRoutingModule
  ]
})
export class PreHomeModule { }
