import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pre-home-first',
  templateUrl: './pre-home-first.component.html',
  styleUrls: ['./pre-home-first.component.scss'],
})
export class PreHomeFirstComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() { }

  nextSection() {
    this.router.navigate([`home`]);
  }

}
