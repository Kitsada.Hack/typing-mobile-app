import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pre-home-second',
  templateUrl: './pre-home-second.component.html',
  styleUrls: ['./pre-home-second.component.scss'],
})
export class PreHomeSecondComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() { }

  nextSection() {
    this.router.navigate([`pre-home/third`]);
  }

  backSection() {
    this.router.navigate([`pre-home/first`]);
  }

}
