import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pre-home-third',
  templateUrl: './pre-home-third.component.html',
  styleUrls: ['./pre-home-third.component.scss'],
})
export class PreHomeThirdComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() { }

  nextSection() {
    this.router.navigate([`home`]);
  }

  backSection() {
    this.router.navigate([`pre-home/second`]);
  }

}
