import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreHomeFirstComponent } from './components/pre-home-first/pre-home-first.component';
import { PreHomeSecondComponent } from './components/pre-home-second/pre-home-second.component';
import { PreHomeThirdComponent } from './components/pre-home-third/pre-home-third.component';

const routes: Routes = [
  {
    path: 'first',
    component: PreHomeFirstComponent
  },
  {
    path: 'second',
    component: PreHomeSecondComponent
  },
  {
    path: 'third',
    component: PreHomeThirdComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreHomeRoutingModule { }
