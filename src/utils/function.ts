export const switchLanguage = (lang: any) => {
    return lang === 'TH' ? 'EN' : 'TH';
}
