export const session = [
    {
        name: '1',
        description: [
            {
                target: 'One',
                lang: 'TH',
                sessionTitle: 'ฝึกทักษะการพิมพ์ดีดภาษาไทยวันที่ 1',
                sessionLists: [
                    { label: 'แบบฝึกหัดที่ 4.1 การพิมพ์อักษรแป้นเหย้า ฟ ห ก ด .่ า ส ว', value: '4Dot1', filename: '4_1.txt' },
                    { label: 'แบบฝึกหัดที่ 4.2 การพิมพ์อักษรแป้นเหย้า ผสมการเคาะวรรค', value: '4Dot2', filename: '4_2.txt' },
                    { label: 'แบบฝึกหัดที่ 4.3 การพิมพ์ผสมคำ 1 พยางค์', value: '4Dot3', filename: '4_3.txt' },
                    { label: 'แบบฝึกหัดที่ 4.4 การพิมพ์ผสมคำ 2 พยางค์', value: '4Dot4', filename: '4_4.txt' },
                    { label: 'แบบฝึกหัดที่ 4.5 การพิมพ์ประโยคสั้น', value: '4Dot5', filename: '4_5.txt' },
                    { label: 'แบบฝึกหัดที่ 4.6 การก้าวนิ้วพิมพ์แป้น เ .้ ง ผสมการเคาะขึ้นบรรทัดใหม่', value: '4Dot6', filename: '4_6.txt' },
                    { label: 'แบบฝึกหัดที่ 4.7 การพิมพ์ผสมคำ 1 พยางค์', value: '4Dot7', filename: '4_7.txt' },
                    { label: 'แบบฝึกหัดที่ 4.8 การพิมพ์ผสมคำ 2 พยางค์', value: '4Dot8', filename: '4_8.txt' },
                    { label: 'แบบฝึกหัดที่ 4.9 การพิมพ์ประโยคสั้น', value: '4Dot9', filename: '4_9.txt' },
                    { label: 'แบบฝึกหัดที่ 4.10 การก้าวนิ้วพิมพ์แป้น พ ะ .ั . .ิ', value: '4Dot10', filenameTH: '4_10.txt' },
                    { label: 'แบบฝึกหัดที่ 4.11 การพิมพ์ผสมคำ 1 พยางค์', value: '4Dot11', filename: '4_11.txt' },
                    { label: 'แบบฝึกหัดที่ 4.12 การพิมพ์ผสมคำ 2 พยางค์', value: '4Dot12', filename: '4_12.txt' },
                    { label: 'แบบฝึกหัดที่ 4.13 การพิมพ์ประโยคสั้น', value: '4Dot13', filename: '4_13.txt' }
                ]
            },
            {
                target: 'One',
                lang: 'EN',
                sessionTitle: 'ฝึกทักษะการพิมพ์ดีดภาษาอังกฤษวันที่ 1',
                sessionLists: [
                    { label: 'แบบฝึกหัดที่ 4.1 การพิมพ์อักษรแป้นเหย้า ฟ ห ก ด .่ า ส ว', value: '4Dot1', filename: '4_1.txt' },
                    { label: 'แบบฝึกหัดที่ 4.2 การพิมพ์อักษรแป้นเหย้า ผสมการเคาะวรรค', value: '4Dot2', filename: '4_2.txt' },
                    { label: 'แบบฝึกหัดที่ 4.3 การพิมพ์ผสมคำ 1 พยางค์', value: '4Dot3', filename: '4_3.txt' },
                    { label: 'แบบฝึกหัดที่ 4.4 การพิมพ์ผสมคำ 2 พยางค์', value: '4Dot4', filename: '4_4.txt' },
                    { label: 'แบบฝึกหัดที่ 4.5 การพิมพ์ประโยคสั้น', value: '4Dot5', filename: '4_5.txt' },
                    { label: 'แบบฝึกหัดที่ 4.6 การก้าวนิ้วพิมพ์แป้น เ .้ ง ผสมการเคาะขึ้นบรรทัดใหม่', value: '4Dot6', filename: '4_6.txt' },
                    { label: 'แบบฝึกหัดที่ 4.7 การพิมพ์ผสมคำ 1 พยางค์', value: '4Dot7', filename: '4_7.txt' },
                    { label: 'แบบฝึกหัดที่ 4.8 การพิมพ์ผสมคำ 2 พยางค์', value: '4Dot8', filename: '4_8.txt' },
                    { label: 'แบบฝึกหัดที่ 4.9 การพิมพ์ประโยคสั้น', value: '4Dot9', filename: '4_9.txt' },
                    { label: 'แบบฝึกหัดที่ 4.10 การก้าวนิ้วพิมพ์แป้น พ ะ .ั . .ิ', value: '4Dot10', filenameTH: '4_10.txt' },
                    { label: 'แบบฝึกหัดที่ 4.11 การพิมพ์ผสมคำ 1 พยางค์', value: '4Dot11', filename: '4_11.txt' },
                    { label: 'แบบฝึกหัดที่ 4.12 การพิมพ์ผสมคำ 2 พยางค์', value: '4Dot12', filename: '4_12.txt' },
                    { label: 'แบบฝึกหัดที่ 4.13 การพิมพ์ประโยคสั้น', value: '4Dot13', filename: '4_13.txt' }
                ]
            }
        ],
    }
]